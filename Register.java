package com.example.anuja.reall;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import android.util.Log;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends Activity implements Spinner.OnItemSelectedListener {
    EditText firstname, lastname, emailid, username, password, confirmpassword, countrry, phone;
   public String firstname1, lastname1, emailid1, username1, password1, confirmpassword1, country1, phone1, male1, coder, countrycode;
    Spinner countr;
    Button regbut;
    RadioGroup gender;
    RadioButton male, female;
    String fullName = "test";

    String url = "http://ec2-18-219-205-60.us-east-2.compute.amazonaws.com:8080/rlg/api/countryIsoCode";
    String regUrl = "http://ec2-18-219-205-60.us-east-2.compute.amazonaws.com:8080/rlg/api/register";
    boolean valid, network;
    ArrayList<String> CountryName;
    ArrayList<String> Code;
    ArrayList<String> countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        phone = (EditText) findViewById(R.id.editText11);
        gender = (RadioGroup) findViewById(R.id.Gender);
        countrry = (EditText) findViewById(R.id.editText9);
        firstname = (EditText) findViewById(R.id.editText4);
        lastname = (EditText) findViewById(R.id.editText6);
        emailid = (EditText) findViewById(R.id.editText10);
        password = (EditText) findViewById(R.id.editText13);
        username = (EditText) findViewById(R.id.editText12);
        confirmpassword = (EditText) findViewById(R.id.editText14);
        countr = (Spinner) findViewById(R.id.ed);
        regbut = (Button) findViewById(R.id.register);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);


        // regbut = (Button) findViewById(R.id.register);
        // country1=countr.getText().toString;
        loadSpinner(url);
        CountryName = new ArrayList<>();
        Code = new ArrayList<>();
        countryCode = new ArrayList<>();


        fun();


        regbut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
               regVol();
                firstname1 = firstname.getText().toString();
                phone1 = phone.getText().toString();
                lastname1 = lastname.getText().toString();
                emailid1 = emailid.getText().toString();
                username1 = username.getText().toString();
                password1 = password.getText().toString();
                confirmpassword1 = confirmpassword.getText().toString();
                country1 = countrry.getText().toString();
                male1 = male.getText().toString();
                valid = validate();
                fun();
//                 Log.w("FirstName",firstname1);
//                 Log.w("LastName",lastname1);
//                 Log.w("country",country1);
//                 Log.w("E-mailId",emailid1);
//                 Log.w("Phone",phone1);
//                 Log.w("UserName",username1);
//                 Log.w("UserName",username1);
//                 Log.w("fullName",fullName);
                //Log.w("GENDER",firstname1);



            }
        });


    }

    String dob = "123", roleCode = "a", gend = "male";

    //int userRole=0,userId=0;

public void regVol(){

    RequestQueue queue = Volley.newRequestQueue(this);
    Response.Listener<String> jsonListerner = new Response.Listener<String>() {
        @Override
        public void onResponse(String list) {

        }
    };

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.w("Volley Error", error.getMessage());
        }
    };
        StringRequest postRequest= new StringRequest(Request.Method.POST,regUrl, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                // response
                Log.d("Response", response);
            }
        },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", "error");



                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String,String> params = new HashMap<String,String>();

                Log.w("UserName","YAAAAAAAAAAAAAYYYYYYYYY");
                params.put("firstName",firstname1);
                params.put("lastName",lastname1);
                params.put("username",username1);
                params.put("password",password1);
                params.put("birthDate","555");
                params.put("gender",gend);
                params.put("fullName",fullName);
                params.put("emailId",emailid1);
                params.put("phoneNo",phone1);
                params.put("address","ppp");
                params.put("country",country1);
                params.put("code",countrycode);
                params.put("countryCode",coder);





                return params;
            }
            public Map<String, String> getHeaders() throws AuthFailureError{
                HashMap<String, String> params = new HashMap<String,String>();
               // params.put("Content-Type", "Application/Json");
                return params;
            }
            public String getBodyContentType() {
                return "application/json";
            }
        };
    queue.add(postRequest);
}


void loadSpinner(String url)
{

    RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());

    StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        @Override

        public void onResponse(String response) {
            try{

                JSONArray json = new JSONArray(response);


                for(int i=0;i<json.length();i++){
                    JSONObject e = json.getJSONObject(i);
                    String country=e.getString("country");
                    String code=e.getString("countryCode");
                    String coder=e.getString("code");
                    CountryName.add(country);
                    Code.add(code);
                    countryCode.add(coder);


                }
                coder=Code.get(0);//countrycode for phone no.
                countrycode=countryCode.get(0);//countrykey

                countr.setAdapter(new ArrayAdapter<String>(Register.this, android.R.layout.simple_spinner_dropdown_item, CountryName));
                countr.setOnItemSelectedListener(Register.this);






            }catch (JSONException e){e.printStackTrace();}
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
        }
    });

    requestQueue.add(stringRequest);

}


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //Setting the values to textviews for a selected item
        String country = CountryName.get(position);
                       Toast.makeText(getApplicationContext(), country, Toast.LENGTH_LONG).show();
        String code=Code.get(position);
        phone.setText(code);
        countrry.setText(country);
    }

    //When no item is selected this method would execute
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
//        textViewName.setText("");
//        textViewCourse.setText("");
//        textViewSession.setText("");
    }


public void fun()
{
    System.out.println(firstname+"sasssssssssssssssss");
}

public boolean validate(){
        boolean valid=true;
        if (firstname1.isEmpty())
        {
        firstname.setError("please enter your firstname!");
        valid=false;
        }
        else if (lastname1.isEmpty() )
        {
        lastname.setError("Please enter lastname!");
        valid=false;
        }
        if (emailid1.isEmpty())
        {
        emailid.setError("Please enter your Email!");
        valid=false;
        }
        if (username1.isEmpty())
        {
        username.setError("Please enter !");
        valid=false;
        }
        if (password1.isEmpty())
        {
        lastname.setError("Please enter password!");
        valid=false;
        }
       /* if (confirmpassword1.isEmpty())
        {
        confirmpassword.setError("Please enter password!");
        valid=false;
        }
        if(!password1.equals(confirmpassword1))
        {
        confirmpassword.setError("username password not matching!");
        valid=false;
        }*/


        return valid;
        }
private boolean isNetworkAvailable()
        {
        ConnectivityManager connectivityManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo=connectivityManager.getActiveNetworkInfo();
        return activeNetInfo!=null && activeNetInfo.isConnected();
        }
        }

//        class Async extends AsyncTask<Void,Void,Void>{
//            protected void onPreExecute() {
//                // TODO Auto-generated method stub
//                super.onPreExecute();
//            }
//            protected Void doInBackground(Void... params) {
//                // TODO Auto-generated method stub
//                //insert_to_server();
//                return null;
//                HttpPost httppost=new HttpPost("http://192.168.1.13/mywebservice/insert_book.php");
//            }
//            protected void onPostExecute(Void result) {
//                // TODO Auto-generated method stub
//                super.onPostExecute(result);
//            }
//
//            public void send(){
//                HttpParams params = new BasicHttpParams();
//                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
//                        HttpVersion.HTTP_1_1);
//                HttpClient httpClient = new DefaultHttpClient(params);
//
//
//                 HttpPost httppost=new HttpPost("http://192.168.1.13/mywebservice/insert_book.php");
//            }
//
//
//        }
